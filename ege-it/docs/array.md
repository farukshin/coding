## С2. Алгоритмы обработки массивов

* Подряд идущие пары элементов массива
* Анализ массива с накопителем
* Поиск максимального элемента
* Другие алгоритмы([#2906](../tasks/2906/Readme.md), [#2917](../tasks/2917/Readme.md), [#2932](../tasks/2932/Readme.md))
* Поиск минимального элемента