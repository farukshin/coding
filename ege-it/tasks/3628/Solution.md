## Решение

Оценка сложности алгоритма О(n):

```
#include <iostream>
using namespace std;

int main(){
    int x = 0, prex = 0, N = 0, maxH = 0, H = 0;
    do{
        prex = x;
        cin >> x;
        if(x == 0){
            maxH = max(maxH, H);
            continue;
        }
        N++;
        if(prex < x && N != 1){
            H += x - prex;
        }else if(N != 1){
            maxH = max(maxH, H);
            H = 0;
        }
    } while (x != 0);
    cout << "Polu4eno " << N << " 4isel\n";
    cout << "Vysota: " << maxH << '\n';
    return 0;
}
```