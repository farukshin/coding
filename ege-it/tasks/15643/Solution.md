## Решение

Програма на языке программирования Go (ver.1.12.9)
Оценка сложности алгоритма О(n):

```
package main

import (
	"fmt"
	"os"
)

func main() {
	var N, cur, res int
	var k34, k17, k2 int = 0, 0, 0
	fmt.Fscan(os.Stdin, &N)
	for i := 1; i <= N; i++ {
		fmt.Fscan(os.Stdin, &cur)
		if cur%34 == 0 {
			k34++
		} else if cur%17 == 0 {
			k17++
		} else if cur%2 == 0 {
			k2++
		}
	}
	res = N*(N-1)/2 - k34*(k34-1)/2 - k34*(N-k34)/2 - k17*k2
	fmt.Println(res)
}

```