## Решение

Програма на языке программирования Go (ver.1.12.9)
Оценка сложности алгоритма О(n):

```
package main

import (
	"fmt"
	"os"
)

func main() {
	var N, cur int
	var k26, k13, k2 int = 0, 0, 0
	var res int = 0

	fmt.Fscan(os.Stdin, &N)
	for i := 1; i <= N; i++ {
		fmt.Fscan(os.Stdin, &cur)
		if cur%26 == 0 {
			k26++
		} else if cur%13 == 0 {
			k13++
		} else if cur%2 == 0 {
			k2++
		}
	}
	res = k26*(k26-1) + k26*(N-k26) + k13*k2
	fmt.Println(res)
}

```