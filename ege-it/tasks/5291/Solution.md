## Решение

Оценка сложности алгоритма О(n):

```
#include <iostream>
using namespace std;

int main(){
    int n =0, x =0, up = 0;
    int last = 1001, start = 1001;
    bool fl = false;
    do{
        cin >> x;
        n++;
        if (x < last){
            if (last > 2*start)
                up++;
            start = x;
        }
        last = x;
    }while(x != 0);
    cout << "4isel: " << n-1 << endl;
    cout << "zna4itel'nyh pod'emov: " << up << endl;
    return 0;
}
```