## Решение

Програма на языке программирования Go (ver.1.12.9)

```
package main

import (
	"fmt"
	"os"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func abs(a int) int {
	if a < 0 {
		return -1 * a
	}
	return a
}

func main() {
	var x, y int
	var maxTopY, minTopY, maxTopH int = 0, 0, 0
	var maxBottY, minBottY, maxBottH int = 0, 0, 0
	var N int
	var s float64
	fmt.Fscan(os.Stdin, &N)

	for i := 1; i <= N; i++ {
		fmt.Fscan(os.Stdin, &x, &y)

		if y > 0 {
			if x == 0 {
				if minTopY == 0 {
					minTopY = y
				}
				maxTopY = max(maxTopY, y)
				minTopY = min(minTopY, y)
			} else {
				maxTopH = max(maxTopH, abs(x))
			}

		}

		if y < 0 {
			if x == 0 {
				if maxBottY == 0 {
					maxBottY = y
				}
				maxBottY = max(maxBottY, y)
				minBottY = min(minBottY, y)
			} else {
				maxBottH = max(maxBottH, abs(x))
			}
		}
	}
	s = float64(max((maxTopY-minTopY)*maxTopH, (maxBottY-minBottY)*maxBottH)) / 2
	fmt.Println(s)
}

```