#include <iostream>
using namespace std;

int main(){
    long N, i, a, b, t;
    long X = 0, Y = 0;
    cin >> N;
    cin >> t;
    for (i=1; i<= N; i++){
        cin >> a >> b;
        X += a;
        Y = min(X + t, Y + b);
    }
    cout << min(X,Y) << '\n';
    return 0;
}