#include <iostream>
#include <list>
using namespace std;

int main(){
    list<int> s1;
    list<int> s2;
    int N=0, i=0, cur=0;
    cin >> N;
    for (i=1; i<= N; i++){
        cin >> cur;
        if(cur<0)
            s1.push_back(cur);
        if(cur>0)
            s2.push_back(cur);
    }
    for (int cur : s1)
        cout << cur << "\n";
    for (int cur : s2)
        cout << cur << "\n";
    return 0;
}