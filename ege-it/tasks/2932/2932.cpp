#include <iostream>
using namespace std;

int main(){
    int x, y, curX, curY;
    curX =0;
    curY = 0;

    cin >> x >> y;
    int a[2] = {-1, 1};
    int b[2] = {-2, 2};

    for(int i : a){
        for(int j : b){
            curX = x + i;
			curY = y + j;
			if (curX <= 8 && curX >= 1 && curY <= 8 && curY >= 1) {
				cout << curX << " " << curY << '\n';
			}

			curX = x + j;
			curY = y + i;
			if (curX <= 8 && curX >= 1 && curY <= 8 && curY >= 1) {
				cout << curX << " " << curY << '\n';
			}
        }

    }
}