## Решение

```
package main

import (
	"fmt"
	"os"
)

func main() {
	var x, y int
	var curX, curY int = 0, 0

	fmt.Fscan(os.Stdin, &x, &y)
	a := [...]int{-1, 1}
	b := [...]int{-2, 2}

	for _, i := range a {
		for _, j := range b {
			curX = x + i
			curY = y + j
			if curX <= 8 && curX >= 1 && curY <= 8 && curY >= 1 {
				fmt.Println(curX, curY)
			}

			curX = x + j
			curY = y + i
			if curX <= 8 && curX >= 1 && curY <= 8 && curY >= 1 {
				fmt.Println(curX, curY)
			}
		}
	}
}

```