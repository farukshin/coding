## Решение

Оценка сложности алгоритма О(n):

```
#include <iostream>
using namespace std;

int main(){
    int N, R, i, x;
    int max1=0, max2 =0;
    int R0 = -1;
    cin >> N;
    for (i=1; i<= N; i++){
        cin >> x;
        if(x%2==0){
            max2 = max(max2, x);
        }else
            max1 = max(max1, x);
    }
    cin >> R;
    if(max1 > 0 && max2 > 0){
        R0 = max1 + max2;
        cout << "Вычисленное контрольное значение" << R0 << '\n';
    }
    if (R0 == R){
        cout << "Контроль пройден" << '\n';
    }else
        cout << "Контроль не пройден" << '\n';
    return 0;
}
```