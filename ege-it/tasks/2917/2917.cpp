#include <iostream>
using namespace std;

#define N 30

int main(){
    long res;
    int i, mas[N];
    res = 1;
    
    for (i=0; i<=N-1; i++){
        cin >> mas[i];
        if(mas[i]<0){
            res *= mas[i];
        }
    }
    cout << res << '\n';
    return 0;
}