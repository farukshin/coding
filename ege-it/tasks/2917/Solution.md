## Решение

Оценка сложности алгоритма О(n):

```
package main

import (
	"fmt"
	"os"
)

const N int = 5

func main() {
	var result int = 1
	var array [N]int

	for _, val := range array {
		fmt.Fscan(os.Stdin, &val)
		if val < 0 {
			result = result * val
		}
	}
	fmt.Println("Результат:", result)
}

```