## Решение [#15995](https://inf-ege.sdamgia.ru/problem?id=15995)

Оценка сложности алгоритма О(n):

```
package main

import (
	"fmt"
	"os"
)

const porog int = 5

func main() {
	var array [porog + 1]int
	var N, cur int
	var multiple13, notMultiple13, count int = 0, 0, 0

	fmt.Fscan(os.Stdin, &N)
	for i := 1; i <= porog; i++ {
		fmt.Fscan(os.Stdin, &array[i])
	}
	for i := porog + 1; i <= N; i++ {
		fmt.Fscan(os.Stdin, &cur)
		if array[i%porog]%13 == 0 {
			multiple13++
		} else {
			notMultiple13++
		}
		if cur%13 == 0 {
			count += multiple13 + notMultiple13
		} else {
			count += multiple13
		}
		array[i%porog] = cur
	}
	fmt.Println(count)
}

```