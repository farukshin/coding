package main

import (
	"fmt"
	"os"
)

func main() {
	var N, x, y int
	var xmax, ymax int = 0, 0
	var S float32

	fmt.Fscan(os.Stdin, &N)
	for i := 1; i <= N; i++ {
		fmt.Fscan(os.Stdin, &x, &y)
		if x < 0 {
			x = -1 * x
		}
		if y < 0 {
			y = -1 * y
		}
		if y == 0 && x > xmax {
			xmax = x
		}
		if x == 0 && y > ymax {
			ymax = y
		}
	}
	S = float32(xmax) * float32(ymax) / 2
	if S == 0 {
		fmt.Println("Треугольник не существует")
	} else {
		fmt.Println(S)
	}
}
