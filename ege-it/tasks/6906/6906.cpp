#include <iostream>
using namespace std;

int main(){
    int h1=0, h2=0, x1=0, x2=0;
    int N, i, x, y;
    bool loadX;
    float S=0;
    cin >> N;
    loadX = false;
    for(i=1; i<= N; i++){
        cin >> x >> y;
        if(y == 0){
            if(loadX == false){
                x1 = x;
                x2 = x;
                loadX = true;
            }
            if(x < x1){
                x1 = x;
            }
            if(x > x2){
                x2 = x;
            }
        }else if(y > 0){
            h1 = max(h1,y);
        }else if(y < 0){
            h2 = max(h2,abs(y));
        }        
    }
    S = (x2 - x1) * h1 / 2 + (x2 - x1) * h2 / 2;
    cout << S << '\n';
    return 0;
}
