#include <iostream>
using namespace std;

int main(){
    long N, i, x, y;
    long minX0 = 0, minX1 = 0, maxX0 = 0, maxX1 = 0, h0 = 0, h1 = 0;
    bool loadX0 = false, loadX1 = false;
    float S0 = 0.0, S1 = 0.0;
    cin >> N;
    for (i=1; i<= N; i++){
        cin >> x >> y;
        if(x>0 && y==0){
            if(loadX1 == false){
                minX1 = x;
                maxX1 = x;
                loadX1 = true;
            }
            minX1 = min(minX1, x);
            maxX1 = max(maxX1, x);
        }
        if(x<0 && y==0){
            if(loadX0 == false){
                minX0 = x;
                maxX0 = x;
                loadX0 = true;
            }
            minX0 = min(minX0, x);
            maxX0 = max(maxX0, x);
        }
        if(x < 0 && y != 0){
            h0 = max(h0, abs(y));
        }
        if(x > 0 && y != 0){
            h1 = max(h1, abs(y));
        }
    }
    S0 = (float)h0 * (maxX0 - minX0) / 2;
    S1 = (float)h1 * (maxX1 - minX1) / 2;
    cout << max(S0, S1) << '\n';
    return 0;
}