## Решение

Оценка сложности алгоритма О(n):

```
#include <iostream>
using namespace std;

int main(){
    int N, i, a;
    int k14 = 0, nk14 = 0, k7 = 0, k2 = 0, prek14 = 0, X = 0;
    cin >> N;
    for(i=1; i<= N; i++){
        cin >> a;
        if(a%14==0){
            if(k14 < a){
                prek14 = k14;
                k14 = a;
            }else if(prek14<a)
                prek14 = a;
        }else{
            nk14 = max(nk14, a);
            if(a%7==0)
                k7 = max(k7, a);
        }
        if(a%2)
            k2 = max(k2, a);
    }
    X = max(k14*nk14, k14*prek14);
    X = max(X, k7*k2);
    cout << X << endl;
    return 0;
}
```