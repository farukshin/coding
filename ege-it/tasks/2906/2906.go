package main

import (
	"fmt"
	"os"
)

const N int = 5

func main() {
	var count, curCount int = 0, 0
	var array [N]int

	for _, val := range array {
		fmt.Fscan(os.Stdin, &val)
		if val%2 == 0 {
			curCount++
		} else {
			curCount = 0
		}
		if count < curCount {
			count = curCount
		}
	}
	fmt.Println("Результат: ", count)
}
