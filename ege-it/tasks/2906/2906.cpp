#include <iostream>
using namespace std;

#define N 30

int main(){         
    int max, curN, i, curMax;
    max = 0;
    curMax=0;

    for (i=1; i<=N; i++){
        cin >> curN;
        if (curN%2==0){
            curMax++;
        }else{
            curMax = 0;
        }
        if(max<curMax){
            max = curMax;
        }        
    }
    cout << max << "\n";
    return 0;
}   