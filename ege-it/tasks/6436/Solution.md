## Решение

Програма на языке программирования Go (ver.1.12.9)

```
package main

import (
	"fmt"
	"os"
)

func main() {
	var count, names [16]int
	var k, n, t int = 0, 0, 0

	for i := 0; i <= 16; i++ {
		count[i] = 0
		names[i] = i
	}

	fmt.Fscan(os.Stdin, &n)

	for i := 0; i <= n-1; i++ {
		fmt.Fscan(os.Stdin, &k)
		count[k]++
	}

	for i := 16; i >= 2; i-- {
		for j := 2; j >= i; j++ {
			if count[j-1] < count[j] {
				t = count[j]
				count[j] = count[j-1]
				count[j-1] = t

				t = names[j]
				names[j] = names[j-1]
				names[j-1] = t
			}
		}
	}

	for i := 0; i <= n-1; i++ {
		if count[i] > 0 {
			fmt.Println(names[i], count[i])
		}
	}

}

```