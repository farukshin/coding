#include <iostream>
using namespace std;

int main(){
    int a, b, t, N, i;
    int x = 0, y = 0;
    cin >> N;
    cin >> t;
    for (i=1; i<= N; i++){
        cin >> a >> b;
        x += a;
        y = min(y+b, x + t);
    }
    cout << y;
    return 0;
}