package main

import (
	"fmt"
	"os"
)

func main() {
	var N, R int
	var a [1000]int
	var max int = 0

	fmt.Fscan(os.Stdin, &N)
	for i := 1; i <= N; i++ {
		fmt.Fscan(os.Stdin, &a[i])
	}

	for i := 0; i <= N-1; i++ {
		for j := 1; j <= N; j++ {
			if (a[i]*a[j]%26 == 0) && (a[i]*a[j] > max) {
				max = a[i] * a[j]
			}
		}
	}
	fmt.Fscan(os.Stdin, &R)

	if max == R && max > 0 {
		fmt.Println("Вычисленное контрольное значение: ", R)
		fmt.Println("Контроль пройден")
	} else {
		fmt.Println("Контроль не пройден")
	}
}
