## Решение [#13373](https://inf-ege.sdamgia.ru/problem?id=13373)

Можно заметить, что 26 = 13*2. Поэтому ответом является наибольшее из двух произведений:

a) максимальное нечетное число, кратное 13, умноженное на максимальное четное число из оставшихся;

b) максимальное число, кратное 26, умноженное на максимальное число из оставшихся.

Чтобы не взять в произведении одно и тоже число, можно хранить номер их поступления.

Если вдруг найдем повторяющиеся значения по этому номеру (например, на тесте из 3 элементов 52, 5 и 7 значение максимума и максимального числа кратного 26 совпадает), то найдем предмаксимумы для обоих случаев и возьмем более выгодный из них.

Пример правильной, но неэффективной программы на языке GO (оценка сложности алгоритма О(n^2)).

```
package main

import (
	"fmt"
	"os"
)

func main() {
	var N, R int
	var a [1000]int
	var max int = 0
	
	fmt.Fscan(os.Stdin, &N)
	for i := 1; i <= N; i++ {
		fmt.Fscan(os.Stdin, &a[i])
	}

	for i := 0; i <= N-1; i++ {
		for j := 1; j <= N; j++ {
			if (a[i]*a[j]%26 == 0) && (a[i]*a[j] > max) {
				max = a[i] * a[j]
			}
		}
	}
	fmt.Fscan(os.Stdin, &R)

	if max == R {
		fmt.Println("Вычисленное контрольное значение: ", R)
		fmt.Println("Контроль пройден")
	} else {
		fmt.Println("Контроль не пройден")
	}
}

```

Оптимальное решение по памяти и во времени (оценка сложности алгоритма О(n)):

```
package main

import (
	"fmt"
	"os"
)

func main() {

	var N, variable, R int
	var max, max26, max13, max2, predmax int = 0, 0, 0, 0, 0
	var indexMax, indexMax26, indexMax13, indexMax2 int = 0, 0, 0, 0
	var ans, ans1, ans2 int = 0, 0, 0

	fmt.Fscan(os.Stdin, &N)

	for i := 1; i <= N; i++ {
		fmt.Fscan(os.Stdin, &variable)
		if variable%13 == 0 && variable > max13 {
			max13 = variable
			indexMax13 = i
		}
		if variable%2 == 0 && variable > max2 {
			max2 = variable
			indexMax2 = i
		}
		if variable%26 == 0 && variable > max26 {
			max26 = variable
			indexMax26 = i
		}
		if variable >= max {
			predmax = max
			max = variable
			indexMax = i
		}
		if variable < max && variable >= predmax {
			predmax = variable
		}
	}

	fmt.Fscan(os.Stdin, &R)

	if indexMax13 != indexMax2 {
		ans1 = max13 * max2
	}

	if indexMax26 != indexMax {
		ans2 = max26 * max
	} else {
		ans2 = predmax * max26
	}

	if ans1 >= ans2 {
		ans = ans1
	} else {
		ans = ans2
	}

	if ans == R && ans > 0 {
		fmt.Println("Вычисленное контрольное значение: ", ans)
		fmt.Println("Контроль пройден")
	} else {
		fmt.Println("Контроль не пройден")
	}
}
```