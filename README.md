# Программирование

Перенес проект на github

cses -> https://github.com/farukshin/cses

stepik -> https://github.com/farukshin/stepik

qt_edu -> https://github.com/farukshin/qt_edu

codeforces -> https://github.com/farukshin/codeforces

e-olymp -> https://github.com/farukshin/e-olymp


## Линейные алгоритмы

* [Алгоритм Джея Кадана](/theory/Jay_Kadane.md)

## Задачи по программированию

* [CSES Problem Set](./cses/README.md)
* [Codeforces](./codeforces/README.md)
* [E-olymp](./e-olymp/README.md)

## Теория программирования

* [Динамическое программирование](/theory/dynamic.md)
* [Графовые алгоритмы](/theory/graf.md)

## Языки программирования

* [C++](./languages/cpp/README.md)
* * [Базовые типы данных](/languages/cpp/base_type.md)
* * [Массивы](/languages/cpp/array.md)
* * [Vector](/languages/cpp/vector.md)

